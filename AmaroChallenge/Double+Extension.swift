//
//  Double+Extension.swift
//  Teste
//
//  Created by Arturo Sekijima on 05/04/18.
//  Copyright © 2018 Arturo Sekijima. All rights reserved.
//

import Foundation

extension Double {
    public func formatCurrency () -> String {
        let formatter = NumberFormatter()
        formatter.numberStyle = NumberFormatter.Style.currency
        formatter.locale = Locale(identifier: "pt_BR")
        formatter.currencySymbol = "R$ "
        formatter.minimumFractionDigits = 2
        formatter.maximumFractionDigits = 2
        let currency: String
        if let floatLiteralSelf = formatter.string(from: NSNumber(floatLiteral:self)) {
            currency = floatLiteralSelf
        } else {
            currency = ""
        }
        return currency
    }
}

