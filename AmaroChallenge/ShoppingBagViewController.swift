//
//  ShoppingBagViewController.swift
//  Teste
//
//  Created by Arturo Sekijima on 03/04/18.
//  Copyright © 2018 Arturo Sekijima. All rights reserved.
//

import UIKit

class ShoppingBagViewController: UIViewController {
    
    @IBOutlet weak var quantityLabel: UILabel!
    @IBOutlet weak var totalPriceLabel: UILabel!
    
    
    @IBOutlet weak var tableView: UITableView!{
        didSet{
            tableView.register(UINib(nibName: "EmptyBagTableViewCell", bundle : nil), forCellReuseIdentifier: "emptyBagCell")
            tableView.register(UINib(nibName: "ProductBagTableViewCell", bundle : nil), forCellReuseIdentifier: "productCell")
            tableView.dataSource = self
            tableView.delegate = self
            tableView.tableFooterView = UIView(frame: CGRect.zero)
        }
    }
    
    var products : Int {
        return ShoppingCart.shared.products
    }
}

extension ShoppingBagViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if products == 0{
            return 44
        }
        return 90
    }
}

extension ShoppingBagViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return products == 0 ? 1 : products
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell : UITableViewCell
        if products == 0 {
            cell = tableView.dequeueReusableCell(withIdentifier: "emptyBagCell", for: indexPath)
            tableView.separatorStyle = .none
        } else {
            cell = tableView.dequeueReusableCell(withIdentifier: "productCell", for: indexPath)
            tableView.separatorStyle = .singleLine
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let c = cell as? ProductBagTableViewCell {
            let prod = ShoppingCart.shared.product(indexPath.row)
            c.delegate = self
            c.configure(prod)
        }
    }
}

extension ShoppingBagViewController : ProductBagTableViewCellDelegate {
    func didDeleteProduct(sku: String) {
        let alert = UIAlertController(title: "Apagar", message: "Tem certeza que deseja apagar esse item?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Sim", style: .default, handler: { [unowned self] _ in
            self.deleteProduct(sku: sku)
        }))
        alert.addAction(UIAlertAction(title: "Não", style: .cancel, handler: nil))
        self.present(alert, animated: true)
    }
    
    private func deleteProduct(sku : String){
        ShoppingCart.shared.remove(product: sku)
        tableView.reloadData()
        reloadFooterLabels()
    }
    
    func didChangeQuantity(_ quantity: Int, sku: String) {
        ShoppingCart.shared.setQuantity(quantity, for: sku)
        reloadFooterLabels()
    }
    
    func reloadFooterLabels () {
        totalPriceLabel.text = ShoppingCart.shared.totalPrice.formatCurrency()
        quantityLabel.text = String(ShoppingCart.shared.totalQuantity)
    }
    
}

