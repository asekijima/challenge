//
//  ProductListViewController.swift
//  AmaroChallenge
//
//  Created by Arturo Sekijima on 02/04/2018.
//  Copyright © 2018 Arturo Sekijima. All rights reserved.
//

import UIKit

class ProductListViewController: UIViewController {
    
    var products = [Product](){
        didSet{
            self.collectionView.performBatchUpdates({
                let indexSet = IndexSet(integersIn: 0...0)
                self.collectionView.reloadSections(indexSet)
            }, completion: nil)
        }
    }
    var productData = [Product](){
        didSet{
            products = productData
        }
    }
    
    @IBOutlet weak var filterButton: UIBarButtonItem!{
        didSet {
            filterButton.style = .plain
        }
    }
    
    
    @IBOutlet weak var collectionView: UICollectionView! {
        didSet{
            collectionView.register(UINib(nibName: "ProductCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "verticalCell")
            collectionView.delegate = self
            collectionView.dataSource = self
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadProducts()
    }
    
    func loadProducts () {
        ProductAPI.fetchProducts(onSuccess: { [unowned self] products in
            if let prds = products{
                self.productData = prds.filter({$0.sizes.filter({$0.isAvailable}).count > 0})
            }
            }, onError: { error in
                let alert = UIAlertController(title: "Erro", message: error.localizedDescription, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                self.present(alert, animated: true)
        })
    }
    
    @IBAction func onFilter(_ sender: UIBarButtonItem) {
        if filterButton.style == .plain {
            filterButton.style = .done
            products = productData.filter({$0.isOnSale})
        }
        else {
            filterButton.style = .plain
            products = productData
        }
    }
    

}

extension ProductListViewController : UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return products.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "verticalCell", for: indexPath)
        return cell
    }
    
}
extension ProductListViewController : UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if let c = cell as? ProductCollectionViewCell{
            let index = indexPath.row
            let item = products[index]
            c.configure(item)
            c.delegate = self
        }
    }
}

extension ProductListViewController : UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let numberOfColumns: CGFloat = 2
        let width = UIScreen.main.bounds.width
        let xInsets: CGFloat = 4
        let cellSpacing: CGFloat = 1
        
        let cellWidth =  (width / numberOfColumns) - (xInsets + cellSpacing)
        return CGSize(width: cellWidth, height: cellWidth * 2)
    }
}

extension ProductListViewController : ProductCollectionViewCellDelegate {
    
    func onButtonPressed(_ index: Int, _ skuIndex: Int) {
        let product = products[index]
        let sku = product.sizes[skuIndex].sku
        addProductToBag(product: products[index], selectedSku: sku)
    }
    
    func onButtonPressed(_ sender: ProductCollectionViewCell) {
        if let index = collectionView.indexPath(for: sender)?.row {
            let product = products[index]
            let skuIndex = sender.sizeIndex
            let sku = product.sizes[skuIndex].sku
            addProductToBag(product: products[index], selectedSku: sku)
        }
    }
    
    func addProductToBag (product : Product, selectedSku : String){
        let cartProduct = CartProduct(product: product, selectedSku: selectedSku, quantity: 1)
        ShoppingCart.shared.addProduct(cartProduct)
    }
}

