//
//  String+Extension.swift
//  Teste
//
//  Created by Arturo Sekijima on 05/04/18.
//  Copyright © 2018 Arturo Sekijima. All rights reserved.
//

import Foundation

extension String {
    
    var numbersOnly: String {
        return components(separatedBy: CharacterSet.decimalDigits.inverted).joined(separator: "")
    }
    
    func currencyDouble () -> Double {
        let s = Double(self.numbersOnly) ?? 0.0
        return s / 100.0
    }
    
}
