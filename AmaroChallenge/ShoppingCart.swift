//
//  ShoppingCart.swift
//  Teste
//
//  Created by Arturo Sekijima on 03/04/18.
//  Copyright © 2018 Arturo Sekijima. All rights reserved.
//

import UIKit

public class ShoppingCart {
    public static let shared = ShoppingCart()
    private init () {}
    
    private var productData = [CartProduct]()
    public var products : Int {
        return productData.count
    }
    
    public var totalPrice : Double {
        return productData.map({$0.product.actualPrice.currencyDouble() * Double($0.quantity)}).reduce(0, +)
    }
    
    public var totalQuantity : Int {
        return productData.map({$0.quantity}).reduce(0, +)
    }
    
    public func product (_ index : Int) -> CartProduct {
        return productData[index]
    }
    
    private func indexFor (sku : String) -> Int? {
        return productData.index(where: {$0.selectedSku == sku})
    }
    
    public func addProduct (_ product : CartProduct){
        let index = indexFor(sku: product.selectedSku)
        if let i = index {
            productData[i].quantity += 1
        } else {
            productData.append(product)
        }
    }
    
    public func removeProduct (_ product : CartProduct){
        if let index = indexFor(sku: product.selectedSku){
            productData.remove(at: index)
        }
    }
    
    public func setQuantity (_ quantity : Int, for product : String){
        let index = indexFor(sku: product)
        if let i = index {
            productData[i].quantity = quantity
        }
    }
    
    public func remove (product : String) {
        let index = indexFor(sku: product)
        if let i = index {
            productData.remove(at: i)
        }
    }
    
}
