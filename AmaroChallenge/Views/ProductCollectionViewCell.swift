//
//  ProductCollectionViewCell.swift
//  AmaroChallenge
//
//  Created by Arturo Sekijima on 02/04/2018.
//  Copyright © 2018 Arturo Sekijima. All rights reserved.
//

import UIKit

protocol ProductCollectionViewCellDelegate : class {
    func onButtonPressed(_ sender : ProductCollectionViewCell)
}

class ProductCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var saleBadge: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var regularPriceLabel: UILabel!
    @IBOutlet weak var installmentsLabel: UILabel!
    @IBOutlet weak var actualPriceLabel: UILabel!
    @IBOutlet weak var sizeCollection: UICollectionView!{
        didSet{
            sizeCollection.register(UINib(nibName: "ProductSizeCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "sizeCell")
            sizeCollection.delegate = self
            sizeCollection.dataSource = self
        }
    }
    @IBOutlet weak var sizeLabel: UILabel!
    @IBOutlet weak var addToCartButton: UIButton!{
        didSet{
            self.addToCartButton.isEnabled = false
        }
    }
    
    
    @IBOutlet weak var actualPriceAlignConstraint: NSLayoutConstraint!
    @IBOutlet weak var priceInstallmentSpaceConstraint: NSLayoutConstraint!
    @IBOutlet weak var pricesCenteringConstraint: NSLayoutConstraint!
    
    weak var delegate: ProductCollectionViewCellDelegate?
    var productSizes = [ProductSize]()
    var sizeIndex = -1{
        didSet{
            addToCartButton.isEnabled = self.sizeIndex != -1
        }
    }
    
    public func configure(_ product : Product){
        setLayout(for: product.isOnSale)
        setLabels(with: product.name, regularPrice: product.regularPrice, installments: product.installments, actualPrice: product.actualPrice)
        setImage(url: product.imageUrl)
        setSize(product.sizes)
    }
    
    private func setLayout(for sale : Bool){
        if sale {
            actualPriceAlignConstraint.priority = .defaultHigh
            priceInstallmentSpaceConstraint.priority = .defaultHigh
            pricesCenteringConstraint.priority = .defaultLow
        } else {
            actualPriceAlignConstraint.priority = .defaultLow
            priceInstallmentSpaceConstraint.priority = .defaultLow
            pricesCenteringConstraint.priority = .defaultHigh
        }
        saleBadge.isHidden = !sale
        regularPriceLabel.isHidden = !sale
    }
    
    private func setLabels (with name : String, regularPrice : String, installments : String, actualPrice : String){
        nameLabel.text = name
        regularPriceLabel.text = "De \(regularPrice) por"
        installmentsLabel.text = installments
        actualPriceLabel.text = actualPrice
    }
    
    private func setImage(url : String){
        imageView.imageForUrlString(url)
        imageView.alpha = 1
    }
    
    private func setSize(_ sizes : [ProductSize]){
        if sizes.filter({$0.size == "U"}).count > 0 {
            sizeCollection.isHidden = true
            sizeLabel.isHidden = false
            sizeLabel.text = "Tamanho Único"
            sizeIndex = sizes.index(where: {$0.size == "U"}) ?? -1
        } else {
            sizeCollection.isHidden = false
            sizeLabel.isHidden = true
            productSizes = sizes
            sizeCollection.reloadData()
        }
    }
    
    @IBAction func didPressButton(_ sender: Any) {
        if let d = delegate{
            d.onButtonPressed(self)
        }
    }
    

}

extension ProductCollectionViewCell : UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if let c = cell as? ProductSizeCollectionViewCell {
            c.configure(productSizes[indexPath.row])
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        sizeIndex = indexPath.row
    }
}

extension ProductCollectionViewCell : UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return productSizes.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "sizeCell", for: indexPath)
        return cell
    }
}
