//
//  ProductSizeCollectionViewCell.swift
//  Teste
//
//  Created by Arturo Sekijima on 03/04/18.
//  Copyright © 2018 Arturo Sekijima. All rights reserved.
//

import UIKit


class ProductSizeCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var sizeLabel: UILabel!
    override var isSelected: Bool{
        didSet{
            if self.isSelected {
                contentView.backgroundColor = UIColor.lightGray
            } else {
                contentView.backgroundColor = UIColor.clear
            }
        }
    }
    @IBOutlet weak var unavilableLabel: UILabel!
    
    public func configure (_ size : ProductSize){
        sizeLabel.text = size.size
        setLayout(availability: size.isAvailable)
    }
    
    private func setLayout (availability : Bool) {
        isUserInteractionEnabled = availability
        contentView.layer.cornerRadius = CGFloat(roundf(Float(contentView.frame.size.width / 2.0)))
        if !availability {
            unavilableLabel.isHidden = false
            sizeLabel.textColor = UIColor.lightGray
        } else {
            unavilableLabel.isHidden = true
            sizeLabel.textColor = UIColor.black
        }
    }
}
