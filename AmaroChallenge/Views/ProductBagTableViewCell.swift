//
//  ProductBagTableViewCell.swift
//  Teste
//
//  Created by Arturo Sekijima on 03/04/18.
//  Copyright © 2018 Arturo Sekijima. All rights reserved.
//

import UIKit

protocol ProductBagTableViewCellDelegate : class {
    func didDeleteProduct (sku : String)
    func didChangeQuantity (_ quantity : Int, sku : String)
}

class ProductBagTableViewCell: UITableViewCell {
    
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var sizeLabel: UILabel!
    @IBOutlet weak var quantityLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var quantityUpButton: UIButton!
    @IBOutlet weak var quantityDownButton: UIButton!
    
    private var sku : String = ""
    private var quantity : Int = 1{
        didSet {
            if let d = self.delegate {
                d.didChangeQuantity(quantity, sku: sku)
                quantityLabel.text = String(quantity)
            }
        }
    }
    weak var delegate : ProductBagTableViewCellDelegate?
    
    public func configure (_ cartProduct : CartProduct) {
        let prod = cartProduct.product
        productImageView.imageForUrlString(prod.imageUrl)
        nameLabel.text = prod.name
        quantity = cartProduct.quantity
        priceLabel.text = prod.actualPrice
        setSize(sizes: prod.sizes, selected: cartProduct.selectedSku)
        sku = cartProduct.selectedSku
    }
    
    private func setSize (sizes : [ProductSize], selected : String){
        let size = sizes.filter({$0.sku == selected}).first
        if let s = size {
            sizeLabel.text = s.size
        }
    }
    
    @IBAction func onPressUpButton(_ sender: UIButton) {
        quantity += 1
    }
    
    @IBAction func onPressDownButton(_ sender: UIButton) {
        if quantity > 1{
            quantity -= 1
        }
    }
    
    @IBAction func onPressTrashButton(_ sender: UIButton) {
        if let d = delegate {
            d.didDeleteProduct(sku: sku)
        }
    }
    
}
