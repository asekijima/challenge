//
//  Product.swift
//  AmaroChallenge
//
//  Created by Arturo Sekijima on 02/04/2018.
//  Copyright © 2018 Arturo Sekijima. All rights reserved.
//

import Foundation

public struct Product : Decodable {
    public var name : String
    public var style : String
    public var colorCode : String
    public var colorSlug : String
    public var isOnSale : Bool
    public var regularPrice : String
    public var actualPrice : String
    public var discountPercentage : String
    public var installments : String
    public var imageUrl : String
    public var sizes : [ProductSize]
    
    public enum CodingKeys : String, CodingKey {
        case name
        case style
        case colorCode = "code_color"
        case colorSlug = "color_slug"
        case isOnSale = "on_sale"
        case regularPrice = "regular_price"
        case actualPrice = "actual_price"
        case discountPercentage = "discount_percentage"
        case installments
        case imageUrl = "image"
        case sizes
    }
}

public struct ProductSize : Decodable {
    public var isAvailable : Bool
    public var size : String
    public var sku : String
    
    public enum CodingKeys : String, CodingKey {
        case isAvailable = "available"
        case size
        case sku
    }
}

public struct CartProduct {
    public var product : Product
    public var selectedSku : String
    public var quantity : Int
}


