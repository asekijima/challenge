//
//  File.swift
//  Teste
//
//  Created by Arturo Sekijima on 04/04/18.
//  Copyright © 2018 Arturo Sekijima. All rights reserved.
//

import UIKit
import Kingfisher

extension UIImageView {
    public func imageForUrl (_ url : URL){
        self.kf.setImage(with: url, placeholder: UIImage(named: "product_placeholder"), options: nil, progressBlock: nil, completionHandler: nil)
//        DispatchQueue.global().async {
//            if let imageData = try? Data(contentsOf: url){
//                DispatchQueue.main.async {
//                    self.image = UIImage(data: imageData)
//                }
//            }
//        }
    }
    
    public func imageForUrlString (_ url : String){
        guard let uurl = URL(string: url) else {
            return
        }
        imageForUrl(uurl)
    }
}
